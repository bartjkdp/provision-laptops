#!/bin/sh

# Apt dependencies
sudo apt install -y \
    apt-transport-https \
    bridge-utils \
    ca-certificates \
    curl \
    gnupg-agent \
    software-properties-common

# Add Docker repository
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -
sudo bash -c 'echo "deb [arch=amd64] https://download.docker.com/linux/ubuntu disco stable" > /etc/apt/sources.list.d/docker-ce.list'
sudo apt-get update

# More apt dependencies
sudo apt install -y \
    containerd.io \
    docker-ce \
    docker-ce-cli \
    git \
    gnupg2 \
    gnupg-agent \
    gnupg-curl \
    scdaemon \
    pcscd \
    htop \
    libvirt-daemon \
    locate \
    mkchromecast \
    pass \
    python3 \
    python3-pip \
    qemu \
    qemu-kvm \
    texlive \
    virt-manager \
    wireguard \
    zsh

# Snap dependencies
sudo snap install --classic \
    code \
    kubectl \
    kontena-lens \
    slack \
    spotify


# Chrome
wget https://dl.google.com/linux/direct/google-chrome-stable_current_amd64.deb && \
    sudo dpkg -i google-chrome-stable_current_amd64.deb && \
    rm -f google-chrome-stable_current_amd64.deb

# Docker compose
sudo curl -L "https://github.com/docker/compose/releases/download/1.25.0/docker-compose-$(uname -s)-$(uname -m)" -o /usr/local/bin/docker-compose && \
    sudo chmod +x /usr/local/bin/docker-compose

# Go
wget https://dl.google.com/go/go1.15.6.linux-amd64.tar.gz && \
    tar -xvf go1.15.6.linux-amd64.tar.gz && \
    sudo mv go /usr/local && \
    rm go1.15.6.linux-amd64.tar.gz

# Helm
wget https://get.helm.sh/helm-v3.0.2-linux-amd64.tar.gz && \
    tar -xvf helm-v3.0.2-linux-amd64.tar.gz && \
    sudo mv linux-amd64/helm /usr/local/bin && \
    rm -Rf linux-amd64 helm-v3.0.2-linux-amd64.tar.gz

# Keybase
wget https://prerelease.keybase.io/keybase_amd64.deb && \
    sudo dpkg -i keybase_amd64.deb && \
    rm -f keybase_amd64.deb

# kube-ps1
wget https://github.com/jonmosco/kube-ps1/archive/v0.7.0.zip && \
    unzip v0.7.0.zip && \
    sudo mv kube-ps1-0.7.0 /opt/kube-ps1 && \
    rm v0.7.0.zip

# kubectx
wget https://github.com/ahmetb/kubectx/releases/download/v0.9.1/kubectx_v0.9.1_linux_x86_64.tar.gz && \
    tar -xvf kubectx_v0.9.1_linux_x86_64.tar.gz && \
    sudo mv kubectx /usr/local/bin && \
    rm LICENSE kubectx_v0.9.1_linux_x86_64.tar.gz

# Minikube
wget https://storage.googleapis.com/minikube/releases/latest/minikube-linux-amd64 && \
    chmod +x minikube-linux-amd64 && \
    sudo mv minikube-linux-amd64 /usr/local/bin/minikube

# Node
curl -sL https://deb.nodesource.com/setup_12.x | sudo -E bash - && \
    sudo apt install -y nodejs

# Skaffold
curl -Lo skaffold https://storage.googleapis.com/skaffold/releases/latest/skaffold-linux-amd64 && \
    chmod +x skaffold && \
    sudo mv skaffold /usr/local/bin

# Yubico
sudo apt-add-repository ppa:yubico/stable && \
    sudo apt install yubikey-manager

# Add current user to the docker and libvirt groups
sudo usermod -aG docker $USER
sudo usermod -aG libvirt $USER

# Install Monaco font
sudo mkdir -p /usr/share/fonts/truetype/custom && \
    wget -c http://jorrel.googlepages.com/Monaco_Linux.ttf && \
    sudo mv Monaco_Linux.ttf /usr/share/fonts/truetype/custom && \
    sudo fc-cache -f

# Install Oh my zh
sh -c "$(curl -fsSL https://raw.githubusercontent.com/robbyrussell/oh-my-zsh/master/tools/install.sh)"

# Python
pip3 install virtualenvwrapper

# Go
mkdir -p ~/go
mkdir -p ~/go/bin

# Google cloud SDK
echo "deb [signed-by=/usr/share/keyrings/cloud.google.gpg] https://packages.cloud.google.com/apt cloud-sdk main" | sudo tee -a /etc/apt/sources.list.d/google-cloud-sdk.list
curl https://packages.cloud.google.com/apt/doc/apt-key.gpg | sudo apt-key --keyring /usr/share/keyrings/cloud.google.gpg add -
sudo apt-get update && sudo apt-get install google-cloud-sdk

# Copy settings
cp ~/.zshrc ~/.zshrc.original
cp ./dot-gitconfig ~/.gitconfig
cp ./dot-gitignore ~/.gitignore
cp ./dot-zshrc ~/.zshrc
cp ./gpg-conf ~/.gnupg/gpg.conf

# Aliases
alias open="xdg-open"

