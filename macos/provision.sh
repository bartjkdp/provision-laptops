#!/bin/sh

# Install brew dependencies
brew install dnsmasq
brew install orbstack
brew install git
brew install golang
brew install gpg
brew install kube-ps1
brew install kubectl
brew install kubectx
brew install kubernetes-helm
brew install nmap
brew install node@20
brew install pass
brew install pinentry-mac
brew install protobuf
brew install python3
brew install skaffold
brew install telnet
brew install opentofu
brew install tmux
brew install vim
brew install zsh zsh-completions zsh-syntax-highlighting

# Install cask dependencies
brew install --cask 1password
brew install --cask adobe-acrobat-reader
brew install --cask figma
brew install --cask firefox
brew install --cask google-chrome
brew install --cask iterm2
brew install --cask keybase
brew install --cask lens
brew install --cask mactex
brew install --cask microsoft-teams
brew install --cask notable
brew install --cask poedit
brew install --cask slack
brew install --cask rectangle
brew install --cask spotify
brew install --cask vagrant
brew install --cask visual-studio-code
brew install --cask vlc
brew install --cask zoom

# Install Oh my zh
sh -c "$(curl -fsSL https://raw.github.com/robbyrussell/oh-my-zsh/master/tools/install.sh)"

# Go
mkdir -p ~/go
mkdir -p ~/go/bin

# Copy settings
cp ~/.zshrc ~/.zshrc.original
cp ./dot-gitconfig ~/.gitconfig
cp ./dot-gitignore ~/.gitignore
cp ./dot-zshrc ~/.zshrc
cp ./gpg-conf ~/.gnupg/gpg.conf
cp ./gpg-agent-conf ~/.gnupg/gpg-agent.conf
